## 1.0.0

- Initial version

## 1.0.1

- Readme and example was improwed

## 1.0.2

- Example improved

## 1.0.3

- Example fixed

## 1.1.0

- Iterable support

## 1.2.0

- Pipe operator return source argument if f(x)==null

## 2.0.0

- Pipe operator `|` - returns result of function call.
- Inspect operator `/` - calls side-effect function and returns source value.

## 2.1.0

- Pipe operator for `Function` type. Example improved.

## 3.0.0

- Null safety was added.

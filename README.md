Pipe operator for some basic Dart types.

```
"hello" | print;
```

Supported types are: String, Map, Iterable.

I was create this extension, commonly for comfortable variable printing.

[license](https://github.com/dart-lang/stagehand/blob/master/LICENSE).

## Usage

See example for details.

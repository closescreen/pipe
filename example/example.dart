import 'package:pipe/pipe.dart';

void main() {
  // Piping operator `|` is available only for String, Iterable, Map, Null, Function
  // Piping operator `/` is available only for String, Iterable, Map, Null

  // If you want to pipe value through function 
  //  and to return result of function call - use `|` opearator.
  // Else if you want to call side-effect function 
  //  on value and return the same value - use `/` operator. 

  /// `String`:
  var str1 = 'Hello' / print; // prints 'Hello', returns 'Hello' 
  print(str1); // Hello

  var str2 = 'Hello' | print; // prints 'Hello', returns null
  print(str2);


  String? str3 = 'Hello' 
    | (s) => '$s, man!' // returns 'Hello, man!'
    / (s) => print('Yes! $s'); // prints 'Yes! Hello, man!', returns original string 'Hello, man!'
  
  print(str3); // Hello, man!

  /// `List`:
  var list = [1, 2, 3] / (l)=>print('List: $l'); // prints List: [1, 2, 3], returns [1, 2, 3]
  print(list); // [1, 2, 3]

  /// `Map` and `Set` literals may produce syntax error:
  // {'a':123} | print; // syntax error
  // {1,2,3} | print; // syntax error
  // Use variables in this case:

  var m = {'a': 123};
  m / print; // {a: 123}

  var s = {1, 2, 3};
  s / print; // {1, 2, 3}

  // Only single-argument functions are supported on the rigth side of pipe operator.
  // But you can go that way:

  /// Two arguments function:
  String concat(String a, String b) => a+b;
  
  'Hello' | (a) => concat(a, '!') / print; // Hello!

  // Function and pipe:
  myDigit()=>123;
  myDigitCaller(int Function() f)=>f();
  var r = myDigit | myDigitCaller;
  print(r);
  // Remember: you can't pipe digits:
  // myDigit | myCaller | print; // will produce error

  // You can remember that dynamic has no piping operator.
  // 
  myString()=>'123';
  myStringCaller(String Function() f)=>f();
  var dynamic_str = myString | myStringCaller; // str1 has a `dynamic` type
  // dynamic_str | print; // produce error
  String string_str = myString | myStringCaller; // string_str has a `String` type
  string_str / print; // ok, prints '123'
}

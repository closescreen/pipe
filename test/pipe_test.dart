import 'package:pipe/pipe.dart';
import 'package:test/test.dart';

void main() {
    test('String piping', () {
      expect( 'Hello' | (x)=>'$x!', 'Hello!');
    });

    test('List piping',(){
      expect( [1,2,3] | (l)=>l.join(), '123');
    });

    test('Map piping',(){
      expect( {'a':1,'b':2} | (m)=>m.keys, ['a','b']);
    });

    test('Set piping',(){
      expect( {1,2,3} | (s)=>s.join(), '123');
    });

    test('Function piping',(){
      expect( (()=>123) | ((Function f)=>f()), 123);
    });

    test('String inspecting', () {
      expect( 'Hello' / (x)=>null, 'Hello');
    });

    test('List inspecting',(){
      expect( [1,2,3] / (l)=>null, [1,2,3]);
    });

    test('Map inspecting',(){
      expect( {'a':1,'b':2} / (m)=>null, {'a':1,'b':2});
    });

    test('Set inspecting',(){
      expect( {1,2,3} / (s)=>null, {1,2,3});
    });
}

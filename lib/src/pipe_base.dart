// ---------------------- Piping -----------------------------------

extension NullPiping on Null {
  /// Places the left-side `Null x`  into right-side `Function f` as single argument.
  /// Returns the function result `f(x)`
  operator | (Function(String) f) => f(this!);
}

extension StringPiping on String {
  /// Places the left-side `String x`  into right-side `Function f` as single argument.
  /// Returns the function result `f(x)`
  operator | (Function(String) f) => f(this);
}

extension MapPiping on Map {
  /// Places the left-side `Map x` into right-side `Function f` as single argument.
  /// Returns the function result `f(x)`
  operator | (Function(Map) f) => f(this);
}

extension IterablePiping on Iterable {
  /// Places the left-side `Iterable x` into right-side `Function f` as single argument.
  /// Returns the function result `f(x)`
  operator | ( Function(Iterable) f) => f(this);
}

extension FunctionPiping on Function {
  /// Places the left-side `Function x` into right-side `Function f` as single argument.
  /// Returns the function result `f(x)`
  operator | ( Function f) => f(this);
}

//------------------------- Inspecting --------------------------------

extension NullInspecting on Null {
  /// Places the left-side `Null x`  into right-side `Function f` as single argument.
  /// Returns the source x.
  Null operator / (Function(String) f) {
    f(this!);
    return this;
  } 
}

extension StringInspecting on String {
  /// Places the left-side `String x`  into right-side `Function f` as single argument.
  /// Returns the source x.
  String operator / (Function(String) f) {
    f(this);
    return this;
  } 
}

extension MapInspecting on Map {
  /// Places the left-side `Map x` into right-side `Function f` as single argument.
  /// Returns the source x.
  Map operator / (Function(Map) f) {
    f(this);
    return this;
  } 
}

extension IterableInspecting on Iterable {
  /// Places the left-side `Iterable x` into right-side `Function f` as single argument.
  /// Returns the source x.
  Iterable operator / ( Function(Iterable) f) {
    f(this);
    return this;
  } 
}
